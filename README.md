# maclean

```
 maclean

 A script to automate some relatively safe cleaning activities.

 Usage: maclean [option]

 Extra Options:
   -h     Print this Help
   -j     Clean only the junk directories
   -d     Clean only the developer directories
   -c     Clean only the container directories
   -b     Clean only the few basic directories
   -a     Automatically affirm all Cleanings
```

<br>

Just run the script and answer the prompts.<br>

<details><summary>Click here for a tabled index of operations.</summary>

| Operation | Clean | Remove | Paths | Notes |
| --- | --- | --- | --- | --- |
| User-defined Junk |  | ✔ | ~/.adobe , ~/.macromedia | Default targets listed. Edit or add to the array in the script. |
| Thumbnail cache | ✔ |  | ~/.cache/thumbnails | Keep anything accessed within last 2 weeks. |
| $HOME cache | ✔ |  | ~/.cache | Keep select directories*. Keep anything accessed within last 2 weeks. |
| Journal logs | ✔ |  |  | Keep logs from the last 2 weeks. |
| Coredumps |  | ✔ | /var/lib/systemd/coredump | Removal. |
| SNAP data | ✔ | ✔ | ~/.snap , ~/snap , /var/lib/snapd | Cleaning if installed. Removal if uninstalled. |
| Flatpak data | ✔ | ✔ | ~/.var/app , ~/.local/share/flatpak , /var/lib/flatpak , /var/tmp/flatpak-cache* | Cleaning if installed. Removal if uninstalled. |
| Go data | ✔ | ✔ | ~/go , ~/.cache/go-build | Cleaning if installed. Removal if uninstalled. |
| Java cache |  | ✔ | ~/.java/deployment/cache , ~/.config/java/deployment/cache | Removal. |
| npm cache | ✔ | ✔ | ~/.npm | Cleaning if installed. Removal if uninstalled. |
| yarn cache | ✔ | ✔ | ~/.yarn | Cleaning if installed. Removal if uninstalled. |
| rust crates | ✔ | ✔ | ~/.cargo | Cleaning if installed. Removal if uninstalled. |
| ruby gems | ✔ | ✔ | ~/.local/share/gem | Cleaning if installed. Removal if uninstalled. |
| pip cache | ✔ | ✔ | ~/.cache/pip | Cleaning if installed. Removal if uninstalled. |
| pkgfile cache |  | ✔ | /var/cache/pkgfile | Removal if uninstalled. |
| PackageKit cache | ✔ | ✔ | /var/cache/PackageKit , /var/lib/PackageKit | Cleaning if installed. Removal if uninstalled. |
| Pamac data |  | ✔ | /var/tmp/pamac , /var/tmp/pamac-build-"$USER" | Removal if uninstalled. |
| AUR Helper cache | ✔ | ✔ |  | Loop through supported AUR Helpers to invoke their cleaning flags. |
| Package cache | ✔ |  | /var/cache/pacman/pkg | Remove all uninstalled cache, unused repos, and/or all but 2 latest installed cache. |
| Orphan Packages |  | ✔ | /var/cache/pacman/pkg | Remove all orphan packages. Please review before confirmation. |
| pacnew+pacsave files |  ⁿ/ₐ  |  ⁿ/ₐ  |  | Warning Only. Use `pacdiff` to manage. |

</details>

\* Cache Cleanings of $HOME conveniently omit certain directories; mainly those associated with package managers, AUR helpers, browsers or chroots.<br>

### Download and Use

##### Download
```text
curl -O https://gitlab.com/cscs/maclean/-/raw/main/maclean
```

##### Mark Executable:

```text
chmod +x maclean
```

##### Run:

```text
./maclean
```

<br></br>

### Donate  

Everything is free, but you can donate using these:  

[<img src="https://az743702.vo.msecnd.net/cdn/kofi4.png?v=2" width=160px>](https://ko-fi.com/X8X0VXZU) &nbsp;[<img src="https://gitlab.com/cscs/resources/raw/master/paypalkofi.png" width=160px />](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52)
